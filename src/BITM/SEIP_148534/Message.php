<?php
/**
 * Created by PhpStorm.
 * User: Web App Develop-PHP
 * Date: 1/28/2017
 * Time: 11:26 AM
 */

namespace App;


class Message
{

    public static function message($msg = null){

        if(is_null($msg)){

           return self::getMessage();
        }

        else{

            self::setMessage($msg);
        }




    }

    public static function setMessage($msg){

        $_SESSION['message'] = $msg;

    }

    public static function getMessage(){

        $temp = $_SESSION['message'] ;
        $_SESSION['message'] = "";
        return $temp;

    }

}